#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import json


# Procedimiento para enviar datos al archivo y guardarlos
def save_file():
    # Creacion de diccionario con todos los datos pedidos
    # La cancion sera key y los value el resto
    dic = {i[2]: [i[0], i[1], i[3], i[4], i[10], i[13]] for i in datos}
    with open('top50.json', 'w') as nuevo:
        json.dump(dic, nuevo)


# Procedimiento para abrir el archivo
def abrir():
    archivo = open("top50.csv")
    datos = []
    for i, linea in enumerate(archivo):
        linea = linea.split(',')
        datos.append(linea)
    archivo.close()
    return datos


# Procedimiento para sacar los generos mas bailables
def bailables(datos):
    bailables = []
    for i in datos:
        bailables.append(i[6])
    bailables.sort()
    # Son las posiciones en que quedan los generos mas repetidos
    b1 = bailables[0]
    b2 = bailables[1]
    b3 = bailables[2]
    for i in datos:
        if i[6] == b1 or i[6] == b2 or i[6] == b3:
            # Posion en la que se encuentra las mas bailables
            print("Los generos mas bailables son: ", i[3])


# Procedieminto para sacar las canciones mas lentas
def lentas(datos):
    lentas = []
    for i in datos:
        lentas.append(i[4])
    lentas.sort()
    # Son las posiciones en que quedan las canciones mas lentas
    b1 = lentas[0]
    b2 = lentas[1]
    b3 = lentas[2]
    for i in datos:
        if i[4] == b1 or i[4] == b2 or i[4] == b3:
            # Posicion en la que se encuntran las mas lentas
            print("Los canciones mas lentas son: ", i[1])


# Procedimiento para sacar la mediana
def mediana(datos):
    archivo = open("top50.csv")
    ruido = []
    for i, linea in enumerate(archivo):
        lista = linea.split(',')
        # No se utiliza la primera linea
        if i > 0:
            # Para que tome solo los numeros
            ruido.append(int(lista[7]))
        ruido.sort()
    n = int(len(ruido) / 2)
    # Ajunte del indice
    mediana = ruido[n -1]
    print("La mediana es: ", mediana)
    archivo.close()


# Procedimiento para sacar el artista que mas se repite
def repetidos(datos):
    archivo = open("top50.csv")
    datos = []
    artista = []
    rep = 0
    for i, linea in enumerate(archivo):
        lista = linea.split(',')
        datos.append(linea)
        artista.append(lista[2])
        for i in artista:
            temp = rep
            rep = artista.count(i)
            if rep > temp:
                rep_2 = i
    print("Artista que mas se repite es: ", rep_2, "Con: ", rep, "Canciones")
    archivo.close()
    return datos


# Procedieminto para imprimir los artistas
def artistas(datos):
    archivo = open("top50.csv")
    datos = []
    artista = []
    for i, linea in enumerate(archivo):
        lista = linea.split(',')
        datos.append(linea)
        artista.append(lista[2])
        print(artista[i])
    archivo.close()


# Funcion principal
if __name__ == "__main__":
    datos =  abrir()
    artista = artistas(datos)
    repetido = repetidos(datos)
    mediana = mediana(datos)
    bailables = bailables(datos)
    lentas = lentas(datos)


